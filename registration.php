<?php

/**
 * Magento2 Thai Language Pack (th_TH)
 * 
 * Github: https://github.com/easycommerce/magento2-language-th-th
 * 
 * @author Supawut Ardhan <supawut.a@gmail.com>
 * @copyright Copyright © 2016 Supawut
 * @version 100.1.0
 */

\Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::LANGUAGE,
        'easycommerce_th_th',
        __DIR__
);
